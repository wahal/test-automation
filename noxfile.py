import nox

@nox.session
def lint(session):
    session.run("python", "main.py")
